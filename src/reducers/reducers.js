import { combineReducers } from 'redux'
import {
    ADD_TODO,
    FETCH_TODOS,
    INIT_APP,
    FETCH_CATEGORIES,
    CREATE_CATEGORY,
    CREATE_TASK,
    UPDATE_TASK, DELETE_TASK, CHANGE_INDEX
} from '../action/actions'
import  {LOGIN} from '../action/auth'
import {connectRouter} from "connected-react-router";

function todo(state, action) {
    switch (action.type) {
        case ADD_TODO:
            return {
                id: action.id,
                text: action.text,
            }
        default:
            return state
    }
}

function indexCompoment(state = 0,action){
    switch (action.type) {
        case CHANGE_INDEX:
            return action.index;
        default: return state;
    }
}

function visibleCategories(state = {}, action) {
    switch (action.type) {
        case ADD_TODO:
            let visibleTodos = [
                ...state,
                todo(undefined, action)
            ]
            localStorage.setItem("listTodo",JSON.stringify(visibleTodos) );
            return visibleTodos;
        case CREATE_CATEGORY:
            let category = action.category;
            state[category.id] = category;
            console.log('list category after add',state);
            return state;
        case INIT_APP:
            let toDoFromLocal =JSON.parse(localStorage.getItem('listTodo'));
            if(toDoFromLocal){
                return toDoFromLocal;
            }
        case FETCH_CATEGORIES:
            return action.categories;
        case CREATE_TASK:
            state[action.task.category_id].todos.push(action.task);
            return state;
        case UPDATE_TASK:
            let curCategory = state[action.task.category_id];

            for (let i = 0; i< curCategory.todos.length; i++){
                if(curCategory.todos[i].id == action.task.id){
                    curCategory.todos[i].state = action.task.state;

                }
            }
            state[action.task.category_id] = curCategory;
            return state;
        case DELETE_TASK:
            let delCategory = state[action.categoryId];

            for (let i = 0; i< delCategory.todos.length; i++){
                if(delCategory .todos[i].id == action.task){
                    delCategory .todos[i].deleted = 1;

                }
            }
            state[action.categoryId] = delCategory;
            return state;
        default:
            return state
    }
}

function account(state = [], action) {
    switch (action.type) {
        case LOGIN:
            localStorage.setItem("token",action.result.token);
            window.location.replace('/todo-now');
            return state;
        default : return state;
    }
}
const todoApp = combineReducers({
    visibleCategories,account,indexCompoment
})
export default todoApp