import React from 'react';
import {TodoNow} from "./TodoNow";
import connect from "react-redux/es/connect/connect";
import {dateObjToString, objectToArray} from "../util/Common";
import CategoryTodo from "./CategoryTodo";
import {fetchData} from "./rest/category.service";
import {CHANGE_INDEX, fetchCategories} from "../action/actions";

class TodoLate extends TodoNow {
    constructor(props){
        super(props)
        const {dispatch} = this.props;
        dispatch({
            type:CHANGE_INDEX,
            index:4
        });
    }


    initFormCondition(){
        let now = new Date();
        return {
            from: 19990202,
            to: dateObjToString(now,-1),
            state:0
        }
    }



    render() {
        const {visibleCategories,dispatch} = this.props;
        console.log('visibleCategories:',visibleCategories);
        return (
            <div className="row">
                <div className="col-lg-12">
                    <button type="button" className="btn btn-outline-dark" onClick={this.refreshData.bind(this)}>Tải lại</button>

                    {objectToArray(this.props.visibleCategories).map(category=>(
                        <CategoryTodo category={category} visibleAddTaskBtn={false} visibleDoneBtn={true} condition={this.initFormCondition()}/>
                    ))}
                </div>
            </div>
        )
    }

}
function select(state) {
    return {
        visibleCategories: state.visibleCategories
    };
}
export default connect(select)(TodoLate);