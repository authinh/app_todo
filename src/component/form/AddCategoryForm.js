import React from 'react';
import {connect} from "react-redux";
import {CREATE_CATEGORY, createCategory, createTask} from "../../action/actions";

class AddCategoryForm extends React.Component {

    constructor(props){
        super(props);

    }

    render() {

        return (
            <div className={"row mt-2 " + (this.props.visible?'':'d-none')}>
                <div className="col-12">
                    <form className="form-inline">
                        <div className="form-group">
                            <input type="text" ref="name" className="form-control" placeholder="nhập tên danh mục"/>
                        </div>
                        <button type="button" className="btn btn-primary" onClick={e => this.handleClick(e)}>Thêm danh mục</button>
                    </form>
                </div>
            </div>
        )
    }

    handleClick(e) {
        console.log("handle click");
        const name = this.refs.name.value;
        let form = {
            name: name
        }
        let token = localStorage.getItem('token');

        fetch('http://54.89.144.231:3010/api/v1/category/create', {
            method: 'PUT',
            body: JSON.stringify(form),
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'authorization': 'Bearer ' + token
            }
        }).then(res => res.json()
        ).then(json => {
            if(json.code == 1){
                this.refs.name.value = '';
                const {dispatch} = this.props;
                form.id=json.id;
                form.todos = [];
                console.log("new category:",form);
                dispatch(createCategory(form));
                this.props.onAddCatDone();

                console.log("thêm thành công");
            } else {
                alert(json.message);
            }

        }).catch(err => err);

    }

}
function select(state) {
    return {
        visibleCategories: state.visibleCategories
    };
}

export default connect(select)(AddCategoryForm);