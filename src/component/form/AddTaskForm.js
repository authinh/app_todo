import React from 'react';
import {connect} from "react-redux";
import {CREATE_TASK, createTask} from "../../action/actions";
import {dateObjToString, dateToString, stringToDate} from '../../util/Common'

class AddTaskForm extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <div className={"row mt-2 " + (this.props.visible?'':'d-none')}>
                <div className="col-12">
                    <form className="form-inline">
                        <div className="form-group">
                            <input type="text" ref="task" className="form-control" placeholder="công việc"/>
                        </div>
                        <div className={"form-group " + (this.props.todoNow?"d-none":"") }>
                            <input type="text" ref="dueDate" className="form-control" placeholder="ngày dự đinh,vd: 30/01/2020"/>
                        </div>
                        <button type="button" className="btn btn-primary" onClick={e => this.handleClick(e)}>Thêm</button>
                    </form>
                </div>
            </div>
        )
    }

    handleClick(e) {
        console.log("handle click");
        const {dispatch,categoryId,todoNow} = this.props;
        const name = this.refs.task.value;
        let dueDate;
        if(todoNow){
            dueDate = dateObjToString(new Date());
        } else {
            dueDate = stringToDate(this.refs.dueDate.value);
        }
        if(dueDate == null){
            return;
        }
        let form = {
            name: name,
            task: name,
            due_date:dueDate,
            category_id: categoryId,

        }
        let token = localStorage.getItem('token');

        fetch('http://54.89.144.231:3010/api/v1/task/create', {
            method: 'PUT',
            body: JSON.stringify(form),
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'authorization': 'Bearer ' + token
            }
        }).then(res => res.json()
        ).then(json => {
            if(json.code == 1){
                form.id = json.id;
                form.state = 0;
                dispatch(createTask(form));
                this.props.onAddTaskDone();
                console.log("thêm công việc thành công");
                this.refs.task.value = '';
                this.refs.dueDate.value = '';
            } else {
                alert(json.message);
            }

        }).catch(err => err);

    }

}
function select(state) {
    return {
        visibleCategories: state.visibleCategories
    };
}
export default connect(select)(AddTaskForm);