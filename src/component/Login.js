import React from 'react';
import {connect} from "react-redux";
import {doLogin, LOGIN} from "../action/auth";
import {withRouter} from "react-router-dom";

class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {email: "thf", password: ""}
        localStorage.removeItem('token');
    }

    login(e) {
        e.preventDefault();
        console.log("before login")
        let form = {
            email: this.refs.email.value,
            password: this.refs.password.value
        }
        const {dispatch} = this.props;
        doLogin(form, function (result) {
            dispatch({
                type: LOGIN,
                result: result
            })
        });
        console.log("after login")
    }

    validateForm() {
        return true//this.state.email.length > 0 && this.state.password.length > 0;
    }

    render() {
        return (
            <div>
                <h1>Login...</h1>
                <form onSubmit={this.login.bind(this)}>
                    <ul>
                        <li><label>email</label> <input name="email" ref="email"/></li>
                        <li><label>password</label> <input name="password" ref="password" type="password"/></li>
                        <li><a href="/register">Chưa có tài khoản?</a></li>
                        <li>
                            <button disabled={!this.validateForm.bind(this)}>Login</button>
                        </li>
                    </ul>
                </form>
            </div>
        )
    }
}


export default connect()(Login);
