import React from 'react';
import {validateEmail} from "../util/Validate";
import {doRegister} from "../action/auth";

class Register extends React.Component {

    constructor(props) {
        super(props)
        this.state = {email: "thf", password: ""}
    }

    register(e) {
        e.preventDefault();
        console.log("before login")
        if (this.refs.password.value == '' || this.refs.password.value !== this.refs.repassword.value) {
            alert('Mật khẩu không khớp');
            return;
        }
        if (!validateEmail(this.refs.email.value)) {
            alert('Cần nhập đúng địa chỉ email');
            return;
        }
        let form = {
            email: this.refs.email.value,
            password: this.refs.password.value
        }
        doRegister(form);
        console.log("after login")
    }

    validateForm() {
        return true//this.state.email.length > 0 && this.state.password.length > 0;
    }

    render() {
        return (
            <div>
                <h1>Register...</h1>
                <form onSubmit={this.register.bind(this)}>
                    <ol>
                        <li>
                            <label>email</label>
                            <input ref="email" type="text"></input></li>
                        <li><label>mật khẩu</label>
                            <input ref="password" type="password"/></li>
                        <li><label>nhập lại mật khẩu</label>
                            <input ref="repassword" type="password"/></li>
                        <li>
                            <button disabled={!this.validateForm.bind(this)} type={"submit"}>đăng ký</button>
                            <button><a href="/login">quay lại</a></button>
                        </li>
                    </ol>
                </form>
            </div>
        )
    }
}

export default Register;