import React from 'react';
import Todos from "./Todos";
import AddTaskBtn from "./action/AddTaskBtn";
import AddTaskForm from "./form/AddTaskForm";

class CategoryTodo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            visibleTaskForm: false
        }
    }
    showTaskForm(){
        this.setState({
            visibleTaskForm: true
        })
    }
    hideTaskFrom(){
        this.setState({
            visibleTaskForm: false
        })
    }

    render() {
        const {category} = this.props;
        return (
            <div className="panel panel-success border mt-2">
                <div className="panel-heading pb-3 pt-1 bg-primary text-white"><b className="margin-left-10">{category.name}</b><AddTaskBtn visible={this.props.visibleAddTaskBtn==undefined || this.props.visibleAddTaskBtn } onClick={this.showTaskForm.bind(this)}/> </div>
                <AddTaskForm visible={this.state.visibleTaskForm} todoNow={this.props.todoNow}  categoryId={category.id} onAddTaskDone={this.hideTaskFrom.bind(this)}/>
                <Todos todos={category.todos} condition={this.props.condition} visibleDoneBtn={this.props.visibleDoneBtn==undefined || this.props.visibleDoneBtn }/>
            </div>

        )
    }
}

export default CategoryTodo;