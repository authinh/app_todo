import React from 'react';
import {TodoNow} from "./TodoNow";
import {connect} from "react-redux";
import {dateObjToString, objectToArray} from "../util/Common";
import AddCategoryForm from "./form/AddCategoryForm";
import {CHANGE_INDEX, createCategory, fetchCategories} from "../action/actions";
import CategoryTodo from "./CategoryTodo";
import {fetchData} from "./rest/category.service";

class TodoDone extends TodoNow {
    constructor(props){
        super(props)
        const {dispatch} = this.props;
        dispatch({
            type:CHANGE_INDEX,
            index:3
        });
    }

    initFormCondition(){

        return {
            from: 19990202,
            to: 20500202,
            state: 1
        }
    }



    render() {
        const {visibleCategories,dispatch} = this.props;
        console.log('visibleCategories:',visibleCategories);
        return (
            <div className="row">
                <div className="col-lg-12">
                    <button type="button" className="btn btn-outline-dark" onClick={this.refreshData.bind(this)}>Tải lại</button>

                    {objectToArray(this.props.visibleCategories).map(category=>(
                        <CategoryTodo category={category} visibleAddTaskBtn={false} visibleDoneBtn={false} condition={this.initFormCondition()}/>
                    ))}
                </div>
            </div>
        )
    }

}
function select(state) {
    return {
        visibleCategories: state.visibleCategories
    };
}
export default connect(select)(TodoDone);