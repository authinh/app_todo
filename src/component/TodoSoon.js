import React from 'react';
import {TodoNow} from "./TodoNow";
import connect from "react-redux/es/connect/connect";
import {dateObjToString} from "../util/Common";
import {fetchData} from "./rest/category.service";
import {CHANGE_INDEX, fetchCategories} from "../action/actions";

class TodoSoon extends TodoNow {
    constructor(props){
        super(props)
        this.state = {
            visibleCateForm: false,
            todoNow:false
        }
        const {dispatch} = this.props;
        dispatch({
            type:CHANGE_INDEX,
            index:2
        });
    }

    initFormCondition(){
        let now = new Date();
        return {
            from: dateObjToString(now,1),
            to: 20500202,
            state:0
        }
    }




}
function select(state) {
    return {
        visibleCategories: state.visibleCategories
    };
}
export default connect(select)(TodoSoon);