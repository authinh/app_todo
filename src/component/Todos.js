import React from 'react';
import Todo from "./Todo";

class Todos extends React.Component {

    constructor(props){
        super(props)
        this.state = {update:false}
    }

    updateState(){
        this.setState({update:true});
    }

    render() {
        const {todos} = this.props;
        return (
            <table className="table table-hover mt-2">
                <thead>
                <tr>
                    <th>Công việc</th>
                    <th style={{width: "15%"}} className="text-center">Ngày hoàn thành</th>
                    <th style={{width: "15%"}}>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                    {todos.map(todo => {
                        const {condition} = this.props;
                        if(todo.deleted == 1  ) {
                            return ;
                        }
                        if(todo.due_date < condition.from || todo.due_date > condition.to || condition.state != todo.state ){
                            return;
                        }
                        return (
                        <Todo id={todo.id} task={todo.task} dueDate={todo.due_date} state={todo.sate} onUpdateState={this.updateState.bind(this)} categoryId={todo.category_id} visibleDoneBtn={this.props.visibleDoneBtn}/>
                    )})}

                </tbody>
            </table>
        )
    }
}

export default Todos;