import React from 'react';
import {Link} from "react-router-dom";
import {CHANGE_INDEX, fetchCategories} from "../action/actions";
import {connect} from "react-redux";
import {fetchData} from "./rest/category.service";

class Main extends React.Component {
    constructor(props){
        super(props)
        this.state = {index:-1}
        const token = localStorage.getItem('token');
        if (token) {
            this.refreshData();
        }
    }


    initFormCondition(){
        return {
            from: '10001010',
            to: '22221010'
        }
    }

    refreshData(){
        const form = this.initFormCondition();
        const {dispatch} = this.props;
        fetchData(form,function(visibleCategories){
            dispatch(fetchCategories(visibleCategories));
        })
    }


    render() {
        const {indexCompoment} = this.props;
        return (
            <div id="wrapper">
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
                      rel="stylesheet"/>
                <aside id="sidebar-wrapper">
                    <div className="sidebar-brand">
                        <h2>Logo</h2>
                    </div>
                    <ul className="sidebar-nav font-14">
                        <li className={indexCompoment == 1? 'active':''}>
                            <Link to="/todo-now" ><i className="fa fa-circle-o"></i>Công việc hôm nay</Link>
                        </li>
                        <li className={indexCompoment == 2? 'active':''}>
                            <Link to="/todo-soon"  ><i className="fa fa-circle-o"></i>Công việc sắp tới</Link>
                        </li>
                        <li className={indexCompoment == 3? 'active':''}>
                            <Link to="/todo-done"  ><i className="fa fa-check-circle"></i>Công việc đã hoàn thành</Link>
                        </li>
                        <li className={indexCompoment == 4? 'active':''}>
                            <Link to="/todo-late" ><i className="fa fa-circle-o-notch"></i>Công việc đã
                                trễ</Link>
                        </li>
                        <li>
                           <a href="/login"><i className="fa fa-sign-out"></i>Đăng xuất</a>
                        </li>
                    </ul>
                </aside>


                <section id="content-wrapper">
                    {this.props.children}
                </section>

            </div>
        )
    }
}

function select(state) {
    return {
        visibleCategories: state.visibleCategories,
        indexCompoment: state.indexCompoment
    };
}
export default connect(select)(Main);