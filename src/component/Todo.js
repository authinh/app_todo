import React from 'react';
import {dateToString, stringToDate} from "../util/Common";
import {connect} from "react-redux";
import {DELETE_TASK, deleteTask, UPDATE_TASK, updateTask} from "../action/actions";

class Todo extends React.Component {

    removeTask() {
        console.log("removeTask handle click");
        const {dispatch,id,categoryId} = this.props;
        let form = {
            id: id
        }
        let token = localStorage.getItem('token');

        fetch('http://54.89.144.231:3010/api/v1/task/delete', {
            method: 'DELETE',
            body: JSON.stringify(form),
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'authorization': 'Bearer ' + token
            }
        }).then(res => res.json()
        ).then(json => {
            if(json.code == 1){
                dispatch(deleteTask(id,categoryId));
                this.props.onUpdateState();
                console.log("xóa công việc thành công");
            } else {
                alert(json.message);
            }

        }).catch(err => err);

    }

    doneTask() {
        console.log("updateTask handle click");
        const {dispatch,id,name,categoryId,dueDate,state,task} = this.props;
        let form = {
            name: name,
            task: task,
            due_date: dueDate,
            category_id: categoryId,
            state:1,
            id:id

        }
        let token = localStorage.getItem('token');

        fetch('http://54.89.144.231:3010/api/v1/task/update', {
            method: 'POST',
            body: JSON.stringify(form),
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'authorization': 'Bearer ' + token
            }
        }).then(res => res.json()
        ).then(json => {
            if(json.code == 1){
                dispatch(updateTask(form));
                this.props.onUpdateState();
                console.log("update công việc thành công");
            } else {
                alert(json.message);
            }

        }).catch(err => err);

    }

    render() {
        const {id,task,dueDate,state} = this.props;
        return (
            <tr>
                <td>{task}</td>
                <td className="text-center"><span className="label label-info">{dateToString(dueDate)}</span>
                </td>
                <td>
                    <button type="button" className={"btn btn-success btn-sm margin-right-10 "+ (this.props.visibleDoneBtn?'':'d-none')} onClick={this.doneTask.bind(this)}>{state==1? "Chưa xong":"Đã xong"} </button>
                    <button type="button" className="btn btn-danger btn-sm" onClick={this.removeTask.bind(this)}>Xóa</button>
                </td>
            </tr>
        )
    }
}
function select(state) {
    return {
        visibleCategories: state.visibleCategories
    };
}
export default connect(select)(Todo);