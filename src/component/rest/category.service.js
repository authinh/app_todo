import {fetchCategories} from "../../action/actions";

export function fetchData(searchForm,callback){
    let token = localStorage.getItem('token');
    let url = 'http://54.89.144.231:3010/api/v1/task/list?time=1234';
    for(var property in searchForm){
        url+= "&"+property+"="+searchForm[property];
    }
    console.log('token',token);
    if(token==null){
        window.location.replace('/login');
    }
    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'authorization': 'Bearer ' + token
        }
    })
        .then(res => res.json())
        .then(
            (result) => {

                let visibleCategories = {}
                if(!result.data){
                    return;
                }
                result.data.map(
                    e => {
                        let todo = {
                            id: e.id,
                            category_id: e.category_id,
                            task: e.task,
                            due_date: e.due_date,
                            state: e.state,
                            deleted: e.deleted,
                            name: e.name
                        }
                        if(visibleCategories[todo.category_id]){
                            visibleCategories[todo.category_id].todos.push(todo);
                        }else {
                            visibleCategories[todo.category_id] = {
                                id:todo.category_id,
                                name: todo.name,
                                todos: []
                            };
                            visibleCategories[todo.category_id].todos.push(todo);
                        }
                    })
                callback(visibleCategories);

            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {

            }
        )
}