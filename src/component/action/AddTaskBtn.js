import React from 'react';

class AddTaskBtn extends React.Component {

    render() {
        return (
            <button type="button" className={"btn btn-outline-light pull-right margin-right-10 "+(this.props.visible?'':'d-none')} onClick={this.props.onClick}>Thêm công việc</button>
        )
    }
}

export default AddTaskBtn;