import React from 'react';
import AddTaskBtn from "./action/AddTaskBtn";
import CategoryTodo from "./CategoryTodo";
import {CHANGE_INDEX, createCategory, fetchCategories, fetchTodos} from "../action/actions";
import {dateObjToString, objectToArray} from "../util/Common";
import {connect} from "react-redux";
import AddCategoryForm from "./form/AddCategoryForm";
import {withRouter} from "react-router-dom";
import {fetchData} from "./rest/category.service";

export class TodoNow extends React.Component {

    constructor(props){
        super(props);
        let token = localStorage.getItem('token');
        if(token==null){
            window.location.replace('/login');
        }
        this.state = {
            visibleCateForm: false,
            todoNow:true
        }
        const {dispatch} = this.props;
        dispatch({
            type:CHANGE_INDEX,
            index:1
        });
        /*let now = new Date();*/
       /* const form = {
            from: 19990202,
            to: dateObjToString(now)
        }*/
       /* this.fetchData(form,function(){
        })*/
    }
    initFormCondition(){
        let now = new Date();
        return {
            from: dateObjToString(now),
            to: dateObjToString(now),
            state: 0
        }
    }

    refreshData(){
        const form = {
            from: 19990202,
            to: 20500202
        };
        const {dispatch} = this.props;
        fetchData(form,function(visibleCategories){
            dispatch(fetchCategories(visibleCategories));
        })
    }



    showCategoryForm(){
        this.setState({
            visibleCateForm: true
        })
    }
    hideCategoryForm(){
        this.setState({
            visibleCateForm: false
        })
    }

    render() {
        const {visibleCategories,dispatch} = this.props;
        console.log('visibleCategories:',visibleCategories);
        return (
            <div className="row">
                <div className="col-lg-12">
                    <button type="button" className="btn btn-outline-dark" onClick={this.refreshData.bind(this)}>Tải lại</button>
                    <button type="button" className="btn btn-outline-dark pull-right" onClick={this.showCategoryForm.bind(this)}>Thêm danh mục</button>
                    <AddCategoryForm  visible={this.state.visibleCateForm} onAddCatDone={this.hideCategoryForm.bind(this)}/>
                    {objectToArray(this.props.visibleCategories).map(category=>(
                        <CategoryTodo todoNow={this.state.todoNow} category={category} condition={this.initFormCondition()} />
                        ))}
                </div>
            </div>
        )
    }
}

function select(state) {
    return {
        visibleCategories: state.visibleCategories,
        indexCompoment: state.indexCompoment
    };
}
export default connect(select)(TodoNow);