import React from 'react';
import logo from './logo.svg';
import './App.css';
import Main from "./component/Main";
import Login from "./component/Login";
import {Link} from "react-router-dom";

class App extends React.Component {
    render() {
        const token = localStorage.getItem('token');
        let componentIndex;
        let visible = "";
        if (token) {
            visible = "d-none";
        }
    return (
      <>
          <nav className={"navbar navbar-expand-sm bg-light " + visible}>
              <ul className="navbar-nav">
                  <li className="nav-item">
                      <Link className="nav-link" to="/login">Đăng nhập</Link>
                  </li>
                  <li className="nav-item">
                      <Link className="nav-link" to="/register">Đăng ký</Link>
                  </li>
              </ul>
          </nav>
          {this.props.children}
      </>
  )};
}

export default App;
