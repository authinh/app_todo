import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import {applyMiddleware, compose, createStore} from 'redux';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from "react-redux";
import createHistory from 'history/createHashHistory';
import {ConnectedRouter, routerMiddleware} from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import {all, fork} from 'redux-saga/effects';

import todoApp from "./reducers/reducers";
import Login from "./component/Login";
import Register from "./component/Register";
import 'bootstrap/dist/css/bootstrap.min.css';
import Main from "./component/Main";
import TodoNow from "./component/TodoNow";
import TodoSoon from "./component/TodoSoon";
import TodoDone from "./component/TodoDone";
import TodoLate from "./component/TodoLate";

const store = createStore(todoApp);
//const store =  configureStore(/* provide initial state if any */);
const token = localStorage.getItem('token');
let componentIndex;

if (token) {
    componentIndex = Main;
} else {
    componentIndex = Login;
}

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App>
                <Switch>
                    <Route path="/login" component={Login}>
                    </Route>
                    <Route path="/register" component={Register}/>
                    <Main>
                        <Route path="/todo-now" component={TodoNow}/>
                        <Route path="/todo-soon" component={TodoSoon}/>
                        <Route path="/todo-done" component={TodoDone}/>
                        <Route path="/todo-late" component={TodoLate}/>
                    </Main>
                </Switch>
            </App>
        </Router>
    </Provider>

    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
