export const LOGIN = "LOGIN";

export function doLogin(form,callback){
    return fetch('http://54.89.144.231:3010/api/v1/login', {
        method: 'POST',
        body: JSON.stringify(form),
        headers: {
            'Content-Type': 'application/json; charset=utf-8'

        }
    }).then(res => res.json()
    ).then(json => {
        if(json.code == 1){
            alert(json.message);
            localStorage.setItem("token",json.token);
            callback(json)
        } else {
            alert(json.message);
        }
    }).catch(err => err);
    return {
        type: LOGIN,
    };
}

export function doRegister(form){
    return fetch('http://54.89.144.231:3010/api/v1/register', {
        method: 'POST',
        body: JSON.stringify(form),
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }).then(res => res.json()
    ).then(json => {
        if(json.code == 1){
            alert(json.message);
            window.location.replace('/login')
        } else {
            alert(json.message);
        }

    }).catch(err => err);
}