import {LOGIN} from "./auth";

export const FETCH_TODOS = "FETCH_TODOS";
export const FETCH_CATEGORIES = "FETCH_CATEGORIES";
export const TOGGLE_SAVE_FOOD = "TOGGLE_SAVE_FOOD";
export const LIKE_FOOD = "LIKE_FOOD";
export const ADD_TODO ="ADD_TODO";
export const INIT_APP="INIT_APP";
export const CREATE_CATEGORY ="CREATE_CATEGORY";
export const CREATE_TASK = 'CREATE_TASK';
export const UPDATE_TASK = 'UPDATE_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const CHANGE_INDEX = 'CHANGE_INDEX';

export function fetchTodos(todos){
    return {
        type: FETCH_TODOS,
        todos: todos
    };
}
export function fetchCategories(categories){
    return {
        type: FETCH_CATEGORIES,
        categories: categories
    };
}

export function createCategory(category){
    console.log("new category:",category);
    return {
        type: CREATE_CATEGORY,
        category: category
    };
}

export function createTask(newTask){
    console.log("new task:",newTask);
    return {
        type: CREATE_TASK,
        task: newTask
    };
}

export function updateTask(newTask){
    console.log("new task:",newTask);
    return {
        type: UPDATE_TASK,
        task: newTask
    };
}

export function deleteTask(id,categoryId){
    console.log("delete task:",id);
    return {
        type: DELETE_TASK,
        task: id,
        categoryId:categoryId
    };
}

