export function objectToArray(object){
    let array = [];
    for(let property in object){
        array.push(object[property]);
    }
    return array;
}
export function dateToString(date){
    var label = ":day/:month/:year";

    return label.replace(":day",date.substring(6,8))
        .replace(":month",date.substring(4,6))
        .replace(":year",date.substring(0,4));
}
export function stringToDate(string){
    var label = ":year:month:day";
    var date = string.split('/');
    var month = date[1]+'';
    var day = date[0]+'';
    if(month.length == 1){
        month= '0'+month;
    }

    if(day.length == 1){
        day= '0'+day;
    }
    if(date.length != 3){
        alert('sai đạnh dạng ngày');
        return null;
    }
    return label.replace(":day",day)
        .replace(":month",month)
        .replace(":year",date[2]);
}

export function dateObjToString(date,distance){
    let month = date.getMonth()+"";
    if(month.length == 1){
        month= '0'+(date.getMonth()+1);
    }
    let day = (date.getDate()+(distance|| 0))+'';
    if(day.length == 1){
        day= '0'+day;
    }
    return date.getFullYear()+month+day;
}